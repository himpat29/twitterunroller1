import UnrolledPage from '@/components/home/UnrolledPage'
export default [
    {
        path: '/:conversationId',
        name: 'unrolledPage',
        props: true,
        component: UnrolledPage
      }
  ]
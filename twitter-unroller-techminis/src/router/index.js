import homeRoutes from '@/router/home'
import VueRouter from 'vue-router'
// Vue.use(VueRouter);
let router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes: [
      ...homeRoutes
    ]
  })
export default router
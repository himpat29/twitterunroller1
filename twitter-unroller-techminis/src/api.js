import axios from 'axios'
// import app from '@/main'

// const UserRoles = constants.UserRoles
// const client = axios.create({
//   baseURL: '/',
//   json: true
// })
// for avoiding progress bar in frequently called apis eg. autosaving of timeline
// var progressBarBlacklistUrls = [
//   { method: 'post', urlRegex: '^\/(api)\/(interview)\/([0-9]+)\/(timeline)$' },
//   { method: 'post', urlRegex: '^\/(api)\/(interview)\/([0-9]+)\/(ratings)$' }
// ]
// // Start the progress bar when request starts
// client.interceptors.request.use(config => {
//   for (var url of progressBarBlacklistUrls) {
//     let regex = new RegExp(url.urlRegex)
//     if (regex.test(config.url) && url.method === config.method) {
//       return config
//     }
//   }
//   app.$Progress.start() // for every request start the progress
//   return config
// })

// Add axios interceptors to route to home page on token expiry
// client.interceptors.response.use(function (response) {
//   console.log('axios response came back')
//   // Finish progress bar
//   app.$Progress.finish()
//   return response
// }, function (error) {
//   app.$Progress.fail()
//   console.log('error in response')
//   if (error.response && error.response.status === 401) {
//     store.dispatch('logout')
//     window.location = '/'
//   } else {
//     return Promise.reject(error)
//   }
// })

// export default {
//   async execute (method, path, data, params) {
//     // let accessToken = store.state.token
//     return client({
//       method,
//       url: path,
//       data: data,
//       params: params,
//       baseURL: '/',
//       headers: {
//         // Authorization: `Bearer ${accessToken}`
//       }
//     }).then(res => {
//       return res.data
//     })
//   },
//   fetchTweetsData (conversationId) {
//     // console.log('returning conversation data')
//     // return {
//     //         conversationId: conversationId,
//     //         userId: "1",
//     //         user: {

//     //         },
//     //         originalTweet: "11 skills will make you wealthy faster than any self-help guru:",
//     //         tweets: [
//     //             {
//     //                 id: 1,
//     //                 text: "Persuasion You need this skill. You will have to persuade people everyday that they should invest in you, buy from you, or believe in you. This is done by having confidence in yourself. Start working on the skill, it will pay off in the long run. " + conversationId
//     //             },
//     //             {
//     //                 id: 2,
//     //                 text: "Reading People Everything you do involves people. Mastering the ability to tell what people are thinking and feeling will give you an advantage in life. Learn how to read people and it will take you to new heights " + conversationId
//     //             }
//     //         ] 
//     //     }
//     return this.execute('get', 'http://192.168.36.10:8501/api/webhooks/listOfThreads' + tweetId, null)
//   }
// }

export default {
  async fetchPaginatedThreads (conversationId) {
    
    let url = "http://192.168.36.10:8501/api/webhooks/conversation/" + conversationId;

    try {
      let response = await axios.get(url, {
        headers: {
        
        },
      });
      if (response.status == 200) {
        return JSON.parse(JSON.stringify(response.data));
      } else {
        return null;
      }
    } catch (err) {
      // console.log(err)
      return null;
    }
  },
  async fetchOriginalTweet (tweetId) {
    
    let url = "http://192.168.36.10:8501/api/webhooks/tweet/" + tweetId;

    try {
      let response = await axios.get(url, {
        headers: {
        
        },
      });
      if (response.status == 200) {
        return JSON.parse(JSON.stringify(response.data.data[0]));
      } else {
        return null;
      }
    } catch (err) {
      // console.log(err)
      return null;
    }
  }
}